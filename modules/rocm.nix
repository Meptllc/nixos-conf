{ config, pkgs, ... }:

{
  nixpkgs.config = {
    rocmTargets = ["navi22"];
  };

  hardware.opengl.extraPackages = with pkgs; [
    amdvlk

    rocm-opencl-icd
    rocm-opencl-runtime
  ];

  systemd.tmpfiles.rules = [
    "L+    /opt/rocm/hip   -    -    -     -    ${pkgs.rocmPackages.clr}"
  ];
}
