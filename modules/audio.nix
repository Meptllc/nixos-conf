{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    alsa-utils
    pavucontrol
    playerctl
    helvum
  ];

  security.rtkit.enable = true;

  services.pipewire.extraConfig.pipewire = {
    "10-virtual-devices" = {
      "context.objects" = [
        {
          "factory" = "adapter";
          "args" = {
            "factory.name"     = "support.null-audio-sink";
            "node.name"        = "Virtual-Microphone";
            "node.description" = "Virtual Microphone";
            "media.class"      = "Audio/Source/Virtual";
            "audio.position"   = "MONO";
          };
        }
        {
          "factory" = "adapter";
          "args" = {
            "factory.name"     = "support.null-audio-sink";
            "node.name"        = "Virtual-Output";
            "node.description" = "Virtual Output";
            "media.class"      = "Audio/Sink";
            "audio.position"   = "[ FL FR ]";
          };
        }
      ];
    };
  };
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    jack.enable = true;
    pulse.enable = true;
  };

  hardware.pulseaudio = {
    enable = false;
    package = pkgs.pulseaudioFull;
    extraConfig = ''
      # A sink to output.
      load-module module-null-sink sink_name=game_sink sink_properties=device.description=Game-Sink
      load-module module-loopback source="game_sink.monitor" sink="alsa_output.pci-0000_0e_00.4.analog-stereo"

      # Setup an output_to_mic sink that can be used to... output to mic.
      # No echo cancellation here because I use headphones.
      #
      # USB mic     -> Meta-Input -> Application
      #                   ^
      #                   |
      # Application -> Output-to-Mic -> Output (default)
      #
      # load-module module-null-sink sink_name=meta_input sink_properties=device.description=Meta-Input
      # load-module module-loopback source="alsa_input.usb-046d_0825_8054D1E0-02.mono-fallback" sink="meta_input"
      # set-default-source meta_input.monitor

      # load-module module-null-sink sink_name=output_to_mic sink_properties=device.description=Output-to-Mic
      # load-module module-loopback source="output_and_mic.monitor" sink="meta_input"
      # load-module module-loopback source="output_and_mic.monitor" sink="alsa_output.pci-0000_0e_00.4.analog-stereo"
    '';
  };
}
