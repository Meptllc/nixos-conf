{ config, pkgs, ... }:

# This configuration is specific to the fume host.
let
  # https://github.com/PassthroughPOST/VFIO-Tools/blob/fe9a8dc0f4eb3f1c8f37eb8308aea00a95f80519/libvirt_hooks/qemu
  qemuHook = pkgs.writeShellScript "qemu" ''
    #
    # Author: Sebastiaan Meijer (sebastiaan@passthroughpo.st)
    #
    # Copy this file to /etc/libvirt/hooks, make sure it's called "qemu".
    # After this file is installed, restart libvirt.
    # From now on, you can easily add per-guest qemu hooks.
    # Add your hooks in /etc/libvirt/hooks/qemu.d/vm_name/hook_name/state_name.
    # For a list of available hooks, please refer to https://www.libvirt.org/hooks.html
    #

    GUEST_NAME="$1"
    HOOK_NAME="$2"
    STATE_NAME="$3"
    MISC="''${@:4}"

    BASEDIR="$(dirname $0)"

    HOOKPATH="$BASEDIR/qemu.d/$GUEST_NAME/$HOOK_NAME/$STATE_NAME"

    set -e # If a script exits with an error, we should as well.

    # check if it's a non-empty executable file
    if [ -f "$HOOKPATH" ] && [ -s "$HOOKPATH"] && [ -x "$HOOKPATH" ]; then
        eval \"$HOOKPATH\" "$@"
    elif [ -d "$HOOKPATH" ]; then
        while read file; do
            # check for null string
            if [ ! -z "$file" ]; then
              eval \"$file\" "$@"
            fi
        done <<< "$(find -L "$HOOKPATH" -maxdepth 1 -type f -executable -print;)"
    fi
  '';
  pci_devices = [
    "pci_0000_00_03_0"
    "pci_0000_0c_00_0"
    "pci_0000_0c_00_1"
    "pci_0000_0e_00_4"
    "pci_0000_0e_00_3"
  ];
  detach_cmd = builtins.concatStringsSep "\n" (map (s: "${pkgs.libvirt}/bin/virsh nodedev-detach ${s}") pci_devices);
  attach_cmd = builtins.concatStringsSep "\n" (map (s: "${pkgs.libvirt}/bin/virsh nodedev-reattach ${s}") pci_devices);
  startHook = pkgs.writeShellScript "start.sh" ''
    set -x

    # Stop login manager
    ${pkgs.systemd}/bin/systemctl stop greetd.service

    # Unbind VTconsoles
    echo 0 > /sys/class/vtconsole/vtcon0/bind
    echo 0 > /sys/class/vtconsole/vtcon1/bind

    # Unneeded for AMD GPUs. Unbind EFI-Framebuffer
    # echo efi-framebuffer.0 > /sys/bus/platform/drivers/efi-framebuffer/unbind

    ${detach_cmd}

    # Load VFIO Kernel Module
    ${pkgs.kmod}/bin/modprobe vfio-pci
  '';
  revertHook = pkgs.writeShellScript "revert.sh" ''
    set -x

    # Re-Bind PCI devices.
    ${attach_cmd}

    # Reload AMD driver.
    ${pkgs.kmod}/bin/modprobe amdgpu

    # Rebind VT consoles
    echo 1 > /sys/class/vtconsole/vtcon0/bind
    echo 1 > /sys/class/vtconsole/vtcon1/bind

    # Unneeded for AMD GPUs.
    # echo "efi-framebuffer.0" > /sys/bus/platform/drivers/efi-framebuffer/bind

    # Restart login manager
    ${pkgs.systemd}/bin/systemctl start greetd.service
  '';
in
{
  boot.kernelParams = [
    "amd_iommu=on"
    "iommu=pt"
  ];

  boot.kernelModules = [
    "kvm-amd"
    "vfio-pci"
  ];

  virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;

  systemd.services.libvirtd = {
    # scripts use binaries from these packages
    # NOTE: All these hooks are run with root privileges... Be careful!
    path = with pkgs; [ libvirt procps ];
    preStart = ''
      mkdir -p /var/lib/libvirt/hooks
      mkdir -p /var/lib/libvirt/hooks/qemu.d/win10/prepare/begin
      mkdir -p /var/lib/libvirt/hooks/qemu.d/win10/release/end

      ln -sf ${qemuHook} /var/lib/libvirt/hooks/qemu
      ln -sf ${startHook} /var/lib/libvirt/hooks/qemu.d/win10/prepare/begin/start.sh
      ln -sf ${revertHook} /var/lib/libvirt/hooks/qemu.d/win10/release/end/revert.sh
    '';
  };

  environment.systemPackages = with pkgs; [
    virt-manager
  ];

  # https://github.com/NixOS/nixpkgs/issues/9067
  networking.firewall = {
    # Allow DNS and DHCP. This is unideal, but seemed to have broken.
    allowedUDPPorts = [ 53 67 ];
    checkReversePath = false;
  };
}
