# NixOS Configuration

## New Host Setup
* On a system that can access secrets.yaml generate two age keys for use by the
    new system: one for the user, and one for the system `age-keygen -o somekey`
* Add the pub keys into .sops.yaml
* Re-encrypt the secrets.yaml file. `sops -d -i secrets.yaml` then `sops -e -i secrets.yaml`
* Transfer the keys to the new system and place in ~/.config/sops/age/keys.txt
    and /var/lib/sops-nix/key.txt
* On the new host, generate a password for the main user using `openssl passwd -6`, add this as a value into secrets.yaml
* Add hardware-configuration.nix to hosts/HOSTNAME/hardware-configuration.nix
* If using vpn, add config files to a vpn/ directory and update the vpn0.conf link to point to the .ovpn file.
* Run `git update-index --assume-unchanged vpn0.conf`
* Clone nixpkgs into /etc/nixpkgs (We reference local copies occasionally)

## Notes
Some commands:
```
curl https://gitlab.com/Meptl/nixos-conf/-/archive/main/nixos-conf-main.tar.gz -O
curl -L https://github.com/NixOS/nixpkgs/archive/master.zip -O
nixos-install --flake /mnt/etc/nixos#host
```
