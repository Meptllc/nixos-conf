{
  description = "Meptl's NixOS configuration";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixpkgs2111.url = "github:NixOS/nixpkgs/21.11";
    home-manager = {
      url = "github:nix-community/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    sops-nix.url = "github:Mic92/sops-nix";
    tabbed.url = "gitlab:Meptl/tabbed/master";
    musnix.url = "github:musnix/musnix";
    nix-alien.url = "github:thiagokokada/nix-alien";
    meptlpkgs.url = "gitlab:Meptl/meptlpkgs";
    # meptlpkgs.url = "path:/home/yutoo/meptlpkgs";
  };

  outputs = { self, nixpkgs, nixpkgs2111, ... }@inputs:
    let
      system = "x86_64-linux";
      myoverlay = final: prev: {
        nixos2111 = nixpkgs2111.legacyPackages.${prev.system};

        steam = (prev.steam.override {
          extraPkgs = pkgs: with pkgs; [
            gamemode.lib
            gamemode

            # For gamescope
            xorg.libXcursor
            xorg.libXi
            xorg.libXinerama
            xorg.libXScrnSaver
            libpng
            libpulseaudio
            libvorbis
            stdenv.cc.cc.lib
            libkrb5
            keyutils
          ];
        });
      };
      mkSystem = hostName: nixpkgs.lib.nixosSystem {
        inherit system;
        specialArgs = { inherit inputs; inherit hostName; };
        modules = [
          inputs.musnix.nixosModules.musnix
          inputs.sops-nix.nixosModules.sops

          ({ config, pkgs, ... }: { nixpkgs.overlays = [
            myoverlay
          ]; })
          ./configuration.nix
          ./hosts/${hostName}

          inputs.home-manager.nixosModules.home-manager {
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              # Currently unused because it doesn't seem to be working.
              sharedModules = [ inputs.sops-nix.homeManagerModules.sops ];
              users.yutoo = import ./home;
            };
          }
        ];
      };
    in {
      nixosConfigurations = {
        fume = mkSystem "fume";
        bounce = mkSystem "bounce";
      };
    };
}
